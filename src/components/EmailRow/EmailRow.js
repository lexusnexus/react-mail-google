import { Checkbox, IconButton } from "@material-ui/core";
import React from "react";
import "./EmailRow.css";
import StarBorderOutlinedIcon from "@material-ui/icons/StarBorderOutlined";
import LabelImportantOutlinedIcon from "@material-ui/icons/LabelImportantOutlined";
import { useNavigate } from "react-router-dom";

function EmailRow(props) {
  const { inboxProp } = props;
  let time="";
  let mailDateToday = new Date().toLocaleDateString('en-US', { day: 'numeric',month: 'short' }).split(' ').reverse().join(' ');
  
  if (mailDateToday !== inboxProp.mailDateDisplay){
    time = inboxProp.mailDateDisplay;
  } else {
    time = inboxProp.mailTime;
  }
  
  let fromName = inboxProp.payload.headers.find(element => element.name === "From").value.split(" <");
  fromName = fromName[0];
  let bodyPart = "";
  let attArray = [];
  if (inboxProp.payload.mimeType==="multipart/alternative") {
    bodyPart = inboxProp.payload.parts[1].body.data;
  } else if(inboxProp.payload.mimeType==="multipart/mixed") {
    bodyPart = inboxProp.payload.parts[0].body.data;
  } else if(inboxProp.payload.mimeType==="multipart/related") {
    bodyPart = inboxProp.payload.parts[0].parts[1].body.data;
    attArray = inboxProp.attData;
  } else if(inboxProp.payload.mimeType==="text/html") {
    bodyPart = inboxProp.payload.body.data;
  } else {
    bodyPart = inboxProp.payload.body.data;
  }
  
  const navigate = useNavigate();
  const openMail = () => {
    navigate('/mail',{state:{id:1,subject:inboxProp.payload.headers.find(element => element.name === "Subject").value,name:inboxProp.payload.headers.find(element => element.name === "From").value,attArray: attArray,body:bodyPart,time:inboxProp.mailDateTime,type:inboxProp.payload.mimeType}});
  };
  
  return (
    <div onClick={openMail} className="emailRow">
      <div className="emailRow-options">
        <Checkbox />
        <IconButton>
          <StarBorderOutlinedIcon />
        </IconButton>
        <IconButton>
          <LabelImportantOutlinedIcon />
        </IconButton>
      </div>
      <h3 className="emailRow-title">{fromName}</h3>
      <div className="emailRow-message">
        <h4>
          {inboxProp.payload.headers.find(element => element.name === "Subject").value}{" "}
          <span className="emailRow-description"> - {inboxProp.snippet.replace(/&#39;/g, "'").replace(/&lt;/g, "<").replace(/&gt;/g, ">")}</span>
        </h4>
      </div>
      <p className="emailRow-time">{time}</p>
    </div>
  );
}

export default EmailRow;
