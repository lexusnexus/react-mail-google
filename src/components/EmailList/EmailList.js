import { Checkbox, IconButton } from "@material-ui/core";
import React, { useEffect, useState, useRef } from "react";
import "./EmailList.css";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import RedoIcon from "@material-ui/icons/Redo";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import KeyboardHideIcon from "@material-ui/icons/KeyboardHide";
import SettingsIcon from "@material-ui/icons/Settings";
import InboxIcon from "@material-ui/icons/Inbox";
import PeopleIcon from "@material-ui/icons/People";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import Section from "./../Section/Section";
import EmailRow from "./../EmailRow/EmailRow";
import { trackPromise } from 'react-promise-tracker';

function EmailList() {
  const [specificData, setSpecificData] = useState([]);
  const [attData, setAttData] = useState([]);
  const [loginData, setLoginData] = useState(
    localStorage.getItem('loginData')
    
  );
  const [inbox, setInbox] = useState([]);
  let access_token = localStorage.getItem('access_token')
  const timerId = useRef(null)
  const attArray = [];
  
  const myFunc = () => {
    clearTimeout(timerId.current)
    timerId.current = null
    localStorage.removeItem('loginData');
    localStorage.removeItem('access_token');
    localStorage.removeItem('mailPic');
    window.location.href='/';
  }
  
  useEffect(() => {
    timerId.current = window.setTimeout(myFunc, 3600000)
  }, [])
  
  useEffect(() => {
    //?q=category:updates
    trackPromise(fetch(`https://gmail.googleapis.com/gmail/v1/users/`+loginData+`/messages?q=category:updates`,{
      method: "GET",
      headers: {
        "Authorization": `Bearer ${access_token}`,
        'Content-Type': 'application/json',
        'Content-length': "0"
      }
    }).then(result => result.json())
      .then(result => {
        
        let idList = result.messages.map(x => ({id : x.id,date :new Date(parseInt("0x"+x.id.slice(0, -5))).toLocaleString('en-US', { hour12: false })}));
        
        for (let i = 0; i <= 34 ; i++) {
          trackPromise(fetch(`https://gmail.googleapis.com/gmail/v1/users/`+loginData+`/messages/`+idList[i].id,{
            method: "GET",
            headers: {
              "Authorization": `Bearer ${access_token}`,
              'Content-Type': 'application/json',
              'Content-length': "0"
            }
          }).then(result => result.json())
            .then(result => {
              
              let mailDateTime = new Date(parseInt("0x"+result.id.slice(0, -5))).toLocaleString('en-US', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' , hour: '2-digit', minute: '2-digit', hour12: true});
              let mailTime = new Date(parseInt("0x"+result.id.slice(0, -5))).toLocaleTimeString('en-US',{ hour: '2-digit', minute: '2-digit', hourCycle: "h23"});
              let mailDate = new Date(parseInt("0x"+result.id.slice(0, -5))).toLocaleDateString('en-US', { weekday: 'short', day: 'numeric',month: 'short' });
              let mailSort = new Date(parseInt("0x"+result.id.slice(0, -5)));
              let mailDateDisplay = new Date(parseInt("0x"+result.id.slice(0, -5))).toLocaleDateString('en-US', { day: 'numeric',month: 'short' }).split(' ').reverse().join(' ');
              
              result.mailSort = mailSort;
              result.mailDateDisplay = mailDateDisplay;
              result.mailDateTime = mailDateTime;
              result.mailTime = mailTime;
              result.mailDate = mailDate;
              
              if(result.payload.mimeType==="multipart/related") {
                
                const attLength = result.payload.parts.length;
                
                for (let i = 1; i <= (attLength - 1) ; i++) {
                  fetch(`https://gmail.googleapis.com/gmail/v1/users/`+loginData+`/messages/`+result.id+`/attachments/`+result.payload.parts[i].body.attachmentId,{
                    method: "GET",
                    headers: {
                      "Authorization": `Bearer ${access_token}`,
                      'Content-Type': 'application/json',
                      'Content-length': "0"
                    }
                  }).then(resultAtt => resultAtt.json())
                    .then(resultAtt => {
                      let attObj = {mimeType:result.payload.parts[i].mimeType, filename:result.payload.parts[i].filename,data:resultAtt.data};
                      attArray.push(attObj);
                      
                    })
                }
                result.attData = attArray;
              }
              
              setSpecificData(dataRes => [...dataRes,result]);
              
              
            }).catch(error => window.setTimeout(myFunc, 1)));
        }
      }).catch(error => window.setTimeout(myFunc, 1)));
    
  }, [loginData,access_token]);
  
  useEffect(()=>{
    const list = specificData.sort((a, b) => new Date(b.mailSort).getTime() - new Date(a.mailSort).getTime()).map( (element) => {
      return <EmailRow key={element.id} inboxProp={element}/>
        })
        setInbox(list)
        },[specificData])
        /*console.log(specificData);
        console.log(attData)*/
        return (
          <div className="emailList">
            <div className="emailList-settings">
              <div className="emailList-settingsLeft">
                <Checkbox />
                <IconButton>
                  <ArrowDropDownIcon />
                </IconButton>
                <IconButton>
                  <RedoIcon />
                </IconButton>
                <IconButton>
                  <MoreVertIcon />
                </IconButton>
              </div>
              <div className="emailList-settingsRight">
                <IconButton>
                  <ChevronLeftIcon />
                </IconButton>
                <IconButton>
                  <ChevronRightIcon />
                </IconButton>
                <IconButton>
                  <KeyboardHideIcon />
                </IconButton>
                <IconButton>
                  <SettingsIcon />
                </IconButton>
              </div>
            </div>
            <div className="emailList-sections">
              <Section Icon={InboxIcon} title="Primary" color="#d93025" selected />
              <Section Icon={PeopleIcon} title="Social" color="#1A73E8" />
              <Section Icon={LocalOfferIcon} title="Promotions" color="#188038" />
            </div>
            
            <div className="emailList-list">
              {inbox}
            </div>
            
          </div>
        );
}

export default EmailList;
        